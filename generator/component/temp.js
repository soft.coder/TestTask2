import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import <%= name %>Component from './<%= name %>.component';

let <%= name %>Module = angular.module('<%= nameModule %>', [
  uiRouter
])

.component('<%= nameComponent %>', <%= name %>Component)

.name;

export default <%= name %>Module;
