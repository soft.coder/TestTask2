import template from './<%= name %>.html';
import './<%= name %>.less';

let <%= name %>Component = {
  bindings: {},
  template,
  controller: function() {
    'ngInject'
  }
};

export default <%= name %>Component;
