import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import <%= name %>Directive from './<%= name %>.directive';

let <%= name %>Module = angular.module('<%= nameModule %>', [
  uiRouter
])

.directive('<%= nameComponent %>', <%= name %>Directive)

.name;

export default <%= name %>Module;
