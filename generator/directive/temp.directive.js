import template from './<%= name %>.html';
import './<%= name %>.less';

let <%= name %>Directive = function() {
  'ngInject'
  return {
    restrict: 'E',
    scope: {},
    template,
    controller: function($scope) {
      'ngInject'
    },
    link: function($scope, element) {
    }
  };
};

export default <%= name %>Directive;
