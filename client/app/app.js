import $ from 'jquery';
import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import Common from './common/common';
import Components from './components/components';
import AppComponent from './app.component';
import ngAnimate from 'angular-animate';
import 'normalize.css';

angular.module('app', [
    uiRouter,
    Common,
    Components,
    ngAnimate
  ])
  .config(($locationProvider, $urlRouterProvider, $stateProvider) => {
    "ngInject";
    // @see: https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions
    // #how-to-configure-your-server-to-work-with-html5mode
    $locationProvider.html5Mode(true).hashPrefix('!');


    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('productList', {
        url: '/',
        component: 'home',
      })
      .state('success', {
        url: '/success',
        component: 'shSuccess'
      });
  })

  .component('app', AppComponent);
