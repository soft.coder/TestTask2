import angular from 'angular';
import Home from './home/home';
import Success from './success/success';

let componentModule = angular.module('app.components', [
  Home,
  Success
])

.name;

export default componentModule;
