import template from './success.html';
import './success.less';

let successComponent = {
  bindings: {},
  template,
  controller: function() {
    'ngInject'
  }
};

export default successComponent;
