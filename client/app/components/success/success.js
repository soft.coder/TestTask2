import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import successComponent from './success.component';

let successModule = angular.module('sh.success', [
  uiRouter
])

.component('shSuccess', successComponent)

.name;

export default successModule;
