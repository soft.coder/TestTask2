import SuccessModule from './success';
import SuccessComponent from './success.component';
import SuccessTemplate from './success.html';

describe('Success', () => {
  let $rootScope;

  beforeEach(window.module(SuccessModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });


  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(SuccessTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = SuccessComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(SuccessTemplate);
    });
  });
});
