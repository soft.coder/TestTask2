import template from './home.html';
import './home.less';

let homeComponent = {
  bindings: {},
  template,
  controller: function($scope, $http, $state) {
    'ngInject';
    this.onClick = () => {
      if(!Object.keys($scope.form.$error).length) {
        $http.post('http://localhost:3000/', {
          name: this.txtName,
          email: this.txtEmail
        });
        $state.go('success');
      }

    };

  }
};

export default homeComponent;
