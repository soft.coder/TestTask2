import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import homeComponent from './home.component';

let homeModule = angular.module('home', [
  uiRouter
])

.component('home', homeComponent)

.name;

export default homeModule;
