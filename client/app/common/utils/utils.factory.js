let UtilsFactory = function ($window) {
  "ngInject";

  let getRandom = (min, max) => {
    return min + Math.floor(Math.random() * (max - min))
  };
  let getFromStorage = name => {
    let json = $window.localStorage.getItem(name);
    return json ? JSON.parse(json) : null;
  };
  let saveToStorage = (name, data) => {
    $window.localStorage.setItem(name, JSON.stringify(data));
  };

  return {
    getRandom,
    getFromStorage,
    saveToStorage
  };
};

export default UtilsFactory;
