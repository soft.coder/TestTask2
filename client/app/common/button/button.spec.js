import ButtonModule from './button';
import ButtonComponent from './button.component';
import ButtonTemplate from './button.html';

describe('Button', () => {
  let $rootScope;

  beforeEach(window.module(ButtonModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });


  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(ButtonTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = ButtonComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(ButtonTemplate);
    });
  });
});
