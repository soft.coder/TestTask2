import template from './button.html';
import './button.less';

let buttonDirective = function() {
  'ngInject'
  return {
    restrict: 'E',
    scope: {
      text: '@',
      disabled: '=',
      onClick: '&'
    },
    template,
    link: function(scope, element) {
    }
  };
};

export default buttonDirective;
