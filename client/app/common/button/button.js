import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import buttonDirective from './button.directive';

let buttonModule = angular.module('sh.button', [
  uiRouter
])

.directive('shButton', buttonDirective)

.name;

export default buttonModule;
