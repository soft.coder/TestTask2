import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import textFieldDirective from './textField.directive';

let textFieldModule = angular.module('sh.textField', [
  uiRouter
])

.directive('shTextField', textFieldDirective)

.name;

export default textFieldModule;
