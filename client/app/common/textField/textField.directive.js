import template from './textField.html';
import './textField.less';
import imgOkUrl from '../../../assets/ok.svg';
import imgErrorUrl from '../../../assets/error.svg';

let newId = 1;
let textFieldDirective = function($document, $timeout){
  'ngInject';
  return {
    restrict: 'E',
    scope: {
      name: '@',
      placeholder: '@',
      type: '@',
      required: '=',
      data: '='
    },
    template,
    link: function(scope, element) {
      let id = newId++;
      let inputEl = element.find('.text-field__input');
      let textFieldEl = element.find('.text-field');
      scope.fieldName = scope.name || `textFieldName_${id}`;
      scope.imgOkUrl = imgOkUrl;
      scope.imgErrorUrl = imgErrorUrl;
      scope.onClick = () => {
        inputEl.focus();
      };
      scope.onBlur = () => {
        if(inputEl.val()) {
          textFieldEl.addClass('text-field--has-text');
        } else {
          textFieldEl.removeClass('text-field--has-text');
        }
      };

      function clickAway(event) {
        if(!element.find(event.target).length && event.target !== element[0]) {
          textFieldEl.removeClass('text-field--focus');
        }
      }
      $document.on('click', clickAway);

      inputEl.on('focusin', () => {
        textFieldEl.addClass('text-field--focus');
      });
      inputEl.on('focusout', event => {
        textFieldEl.removeClass('text-field--focus');
      });

      scope.$on('$destroy', () => {
        $document.off('click', clickAway);
        inputEl.off('focusin');
        inputEl.off('focusout');
      });
    }
  };
};

export default textFieldDirective;
