import TextFieldModule from './textField';
import TextFieldComponent from './textField.component';
import TextFieldTemplate from './textField.html';

describe('TextField', () => {
  let $rootScope;

  beforeEach(window.module(TextFieldModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });


  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(TextFieldTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = TextFieldComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(TextFieldTemplate);
    });
  });
});
