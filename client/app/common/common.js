import angular from 'angular';
import TextField from './textField/textField';
import Button from './button/button';

let commonModule = angular.module('app.common', [
  TextField,
  Button
])

.name;

export default commonModule;
